#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from discord.ext import commands
from discord import File

from __init__ import spike_logger, init_log_file_handler, except_hook, intents
from spike_command.Naf import Naf
from spike_event.EventCommon import EventCommon
from spike_settings.SpikeSettings import SpikeSettings
from spike_database.Matches import Matches
from spike_model.Match import Match
from spike_naf.XmlGenerator import NafXml
import csv

# Discord client
client = commands.Bot(command_prefix="n!", intents=intents)
client.help_command.dm_help = True

Already_running = False


def globally_check_naf(ctx):
    if ctx.guild.id == 368441333745975306 or ctx.message.author.id in [315120413883760640, 283238159641477120, 147723093102690305]:
        return True
    else:
        return False


client.add_check(globally_check_naf)


@client.command()
@commands.guild_only()
async def score(ctx, round_index):
    cmd = Naf()
    await cmd.score(ctx, round_index)


@client.command()
@commands.guild_only()
async def xml(ctx):
    competition_search = ["R1T", "R2T", "R3T", "R4T", "R5T", "R6T"]
    match_list = []
    naf_match = []
    match_db = Matches()
    for league_id in [92780, 92781, 92840]:
        match_list.extend(match_db.get_matches_in_league(league_id, "pc"))

    for match_data in match_list:
        match = Match(match_data["match_data"])
        ok = next((cmp_name for cmp_name in competition_search if cmp_name in match.get_competition_name()), None)
        if ok is not None:
            naf_match.append(match)
    xml_data = NafXml.generate_from_matches(naf_match, "Kfoged")
    xml_file = open("welsh.xml", "wt")
    xml_file.write(xml_data)
    xml_file.close()
    await ctx.channel.send(file=File("welsh.xml"))


@client.command()
@commands.guild_only()
async def teams(ctx):
    cmd = Naf()
    await cmd.teams_list(ctx)


@client.command()
@commands.guild_only()
async def reg_teams(ctx):
    cmd = Naf()
    registered_teams = cmd.registered_teams()
    with open('reg_teams.csv', 'w') as csv_file:
        fieldnames = ["coach_name", "naf_name", "naf_number", "race"]
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames, delimiter=',', extrasaction="ignore")
        for team in registered_teams:
            writer.writerow(team)
    await ctx.channel.send(file=File("reg_teams.csv"))


@client.command()
@commands.guild_only()
async def gen_cmp(ctx):
    cmd = Naf()
    await cmd.generate_csv_competitions(ctx)
    await ctx.channel.send("Done")


@client.command()
@commands.guild_only()
async def create_cmp(ctx, password):
    cmd = Naf()
    await cmd.create_ingame_competitions(ctx, password)
    await ctx.channel.send("Done")


def main():
    init_log_file_handler("naf_bot")
    client.run(SpikeSettings.get_discord_token())


@client.event
async def on_ready():
    global Already_running
    if not Already_running:
        spike_logger.info("naf_bot connected")
        Already_running = True
    else:
        spike_logger.warning("naf_bot reconnect")


@client.event
async def on_command_error(ctx, error):
    spike_logger.error("on_command_error: {}".format(error))
    event = EventCommon(client)
    await event.on_command_error(ctx, error, "n!")


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
