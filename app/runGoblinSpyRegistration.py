#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from __init__ import spike_logger, init_log_file_handler, except_hook
from goblin_spy_requester.GoblinSpyRequester import GoblinSpyRequester
from spike_database.Contests import Contests
from spike_database.DiscordGuild import DiscordGuild
from spike_database.ResourcesRequest import ResourcesRequest
from spike_utilities.Utilities import Utilities


def main():
    init_log_file_handler("goblin_spy_registration")
    spike_logger.info("Start")
    register_competition_in_goblin_spy()
    register_contest_in_goblin_spy()
    spike_logger.info("Finished")
    sys.exit()


def register_competition_in_goblin_spy():
    excluded_server = Utilities.get_excluded_server()
    common_db = ResourcesRequest()
    guilds_id = Utilities.get_guilds_id()
    for guild in guilds_id:
        try:
            if guild not in excluded_server:
                db = DiscordGuild(guild)
                competitions = db.get_reported_competitions()
                for competition in competitions:
                    try:
                        competition_data = db.get_competition_data(competition)
                        if competition_data is not None:
                            league_name = db.get_league_name(competition_data[7], competition_data[6])
                            if league_name is not None:
                                platform_data = common_db.get_platform(competition_data[6])
                                req = GoblinSpyRequester()
                                req.register_competition(league_name, competition_data[2], platform_data[1])
                            else:
                                message = "Goblin spy activation error:\n"
                                message += "Server: {}\nUnknown league **{}** for **{}**".format(guild, competition_data[7], competition_data[2])
                                spike_logger.info(message)
                        else:
                            message = "Server: {}\nUnable to register {}".format(guild, competition)
                            spike_logger.info(message)
                    except Exception as e:
                        spike_logger.error("Error in register_goblin_spy: {}".format(e))
                        spike_logger.exception("Unexpected error: " + str(sys.exc_info()[0]))
            else:
                spike_logger.info("Ignore server: {}".format(guild))
        except Exception as e:
            spike_logger.error(e)


def register_contest_in_goblin_spy():
    common_db = ResourcesRequest()
    contest_db = Contests()
    contests = contest_db.get_contest_to_register()
    for contest in contests:
        if "/" not in contest["date"] and "?" not in contest["date"]:
            try:
                platform_data = common_db.get_platform(contest["platform_id"])
                contest_date = contest["date"].replace(" ", "T") + ":00Z"
                req = GoblinSpyRequester()
                ret = req.register_contest(contest["league_name"], contest["competition_name"], platform_data[1], contest["contest_id"], contest_date)
                spike_logger.info("{} - {} - {} registered: {}".format(contest["league_name"], contest["competition_name"], contest["contest_id"], str(ret["IsSuccess"])))
            except Exception as e:
                spike_logger.error("Error in register_contest_in_goblin_spy: {}".format(e))
                spike_logger.exception("Unexpected error: " + str(sys.exc_info()[0]))
        else:
            spike_logger.info("Ignoring date format for: {}".format(contest["date"]))


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
