#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import discord

from spike_settings.SpikeSettings import SpikeSettings
from spike_database.ResourcesRequest import ResourcesRequest
from spike_database.ResourcesInit import ResourcesInit


client = discord.Client()

skill_server = [469824070829998080, 469826994461343744, 469824117735030784]
team_server = [469822191857434634, 469822722080505856, 469823592125825034,
               469823897185943576, 469823666612600832, 469823738196787210,
               469823770924941312, 469823810116386817, 469823861664514079,
               469823628628983848]
race_server = [469824479657459725]

injury_server = [474525952345505793]


def main():
    client.run(SpikeSettings.get_discord_token())


@client.event
async def on_ready():
    print("Logged in as: " + client.user.name)
    client.loop.create_task(fill_common_db())


async def fill_common_db():
    await client.wait_until_ready()
    # Init common DB
    db = ResourcesInit()
    db.open_database()
    db.create_tables()
    db.fill_tables()
    db.close_database()
    db = ResourcesRequest()

    # Init specific server settings and DB
    for guild in client.guilds:
        if guild.id in skill_server:
            print("Read skill from: " + guild.name + " - id: " + str(guild.id))
            emo_list = guild.emojis
            for emoji in emo_list:
                emoji_id = "<:{}:{}>".format(emoji.name, str(emoji.id))
                db.add_skill_emoji(emoji.name, emoji_id, emoji.url)
        elif guild.id in team_server:
            print("Read team logo from: " + guild.name + " - id: " + str(guild.id))
            emo_list = guild.emojis
            for emoji in emo_list:
                emoji_name = emoji.name.replace("Logo_", "")
                emoji_id = "<:{}:{}>".format(emoji_name, str(emoji.id))
                db.add_team_emoji(emoji_name, emoji_id, emoji.url)
        else:
            print("Ignore server: " + guild.name + " - id: " + str(guild.id))

    sys.exit()


def except_hook(type, value, traceback):
    print("Critical error")
    print(type)
    print(value)
    print(traceback)


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
