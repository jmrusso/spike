#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pyshorteners

from discord_resources import Resources
from spike_command.BaseCommand import BaseCommand
from spike_database.Bounty import Bounty as BountyDb
from spike_database.ResourcesRequest import ResourcesRequest
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_translation import Translator
from discord_resources.Utilities import DiscordUtilities


class Bounty(BaseCommand):

    def __init__(self, client):
        super(Bounty, self).__init__(client)

    async def bounty_list(self, ctx):
        utils = DiscordUtilities(self.client)
        channel = ctx.message.channel
        rsrc = Resources()
        db = BountyDb()
        common_db = ResourcesRequest()

        if ctx.guild is not None:
            settings = DiscordGuildSettings(ctx.guild.id)
        else:
            settings = DiscordGuildSettings(0)

        user_id = ctx.message.author.id
        bnty_list = db.get_bountied_player_by_user(user_id)
        bounty_emo = rsrc.get_bounty_emoji()
        # /* {bounty_emo}__**Bounty list**__{bounty_emo} */
        output_msg = Translator.tr("#_bounty_command.bounty_list_title", settings.get_language()).format(bounty_emo=bounty_emo)

        for bounty in bnty_list[:10]:
            platform_data = common_db.get_platform(bounty["platform_id"])
            platform = "\n{}".format(platform_data[2])
            player_name = "**{}**".format(bounty["name"])
            team_name = "**{}**".format(bounty["team_name"])
            issue_date = "*{}*".format(bounty["issue_date"])
            # /* {platform} {player_name} from {team_name} since {issue_date} */
            output_msg += Translator.tr("#_bounty_command.bounty_list_item", settings.get_language()).format(platform=platform, player_name=player_name, team_name=team_name, issue_date=issue_date)

        s = pyshorteners.Shortener()
        url = s.isgd.short("https://spike.ovh/mybounty")
        output_msg += "\n\nFull list:\n{}".format(url)
        await utils.send_custom_message(channel, output_msg)
        await utils.delete_message(ctx.message)

