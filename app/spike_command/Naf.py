#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import os

import pyshorteners
from discord import File

from discord_resources.Utilities import DiscordUtilities
from spike_database.CustomCompetitionTeam import CustomCompetitionTeam
from spike_database.Matches import Matches
from spike_database.ResourcesRequest import ResourcesRequest
from spike_exception import DefaultError
from spike_model.Match import Match
from spike_naf.TourmaXmlReader import TourmaXmlReader
from spike_requester.CyanideApi import CyanideApi
from spike_requester.Utilities import Utilities
from spike_translation import Translator

NAF_LEAGUE_NAME = "NAF Official League"
NAF_LEAGUE_ID = 89647
NAF_OFFICIAL_PLATFORM_ID = 1
NAF_OFFICIAL_PLATFORM = "pc"

WELSH_LEAGUES = ["NAF Welsh Championship", "NAF Welsh Championship 2", "NAF Welsh Championship 3"]
WELSH_LEAGUES_IDS = [92780, 92781, 92840]


class Naf:
    @staticmethod
    async def score(ctx, round_index):
        match_db = Matches()
        score_list = []
        for league_id in WELSH_LEAGUES_IDS:
            match_list = match_db.get_matches_in_league(league_id, NAF_OFFICIAL_PLATFORM)
            for match in match_list:
                match = Match(match["match_data"])
                cmp_name = match.get_competition_name().strip()
                if "R{}".format(round_index) in match.get_competition_name():
                    table_index = cmp_name.replace("NAF_Welsh_R{}T".format(round_index), "").strip()
                    table_index = int(table_index)
                    score_data = {
                        "table_index": table_index,
                        "competition": match.get_competition_name(),
                        "coach_home": match.get_coach_home().get_name(),
                        "coach_away": match.get_coach_away().get_name(),
                        "score_home": match.get_score_home(),
                        "score_away": match.get_score_away()
                    }
                    score_list.append(score_data)
        score_list = sorted(score_list, key=lambda i: i["table_index"])
        with open('score.csv', 'w') as csv_file:
            fieldnames = ["coach_home", "coach_away", "score_home", "score_away"]
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames, delimiter=',', extrasaction="ignore")
            for score in score_list:
                writer.writerow(score)
        await ctx.channel.send(file=File("score.csv"))

    @staticmethod
    async def teams_list(ctx):
        channel = ctx.message.channel
        common_db = ResourcesRequest()
        global_team_list = []

        for league in WELSH_LEAGUES:
            team_list = CyanideApi.get_teams(league)
            global_team_list.extend(team_list.get("teams", []))

        nb_team = len(global_team_list)
        if nb_team > 0:
            output_msg = "**NAF Welsh Championship**"

            for team in global_team_list:
                emoji = common_db.get_team_emoji(team["logo"])
                s = pyshorteners.Shortener()
                url = s.isgd.short("https://spike.ovh/team?team_id={}&platform={}".format(team.get("id"), NAF_OFFICIAL_PLATFORM_ID))
                output_msg += "\n{} {} :: **{}** :: <{}>".format(emoji, team.get("team"), team.get("coach", "AI"), url)
                if len(output_msg) >= 1800:
                    await DiscordUtilities.send_custom_message(channel, output_msg)
                    output_msg = ""
            if len(output_msg) >= 0:
                await DiscordUtilities.send_custom_message(channel, output_msg)
        else:
            # /* No team found in **{competition}** */
            raise DefaultError(Translator.tr("#_common_command.no_team_found_error", "en").format(competition="NAF Welsh Championship"))
        await DiscordUtilities.delete_message(ctx.message)

    @staticmethod
    def registered_teams():
        custom_team_db = CustomCompetitionTeam()
        team_list = custom_team_db.get_teams(205772, NAF_OFFICIAL_PLATFORM_ID)

        registered_teams = []
        for team in team_list:
            team = team["members"][0]
            team_model = Utilities.get_team(team_id=team["team_id"], platform_id=NAF_OFFICIAL_PLATFORM_ID, cached_data=True)
            if team_model is None:
                team_model = Utilities.get_team(team_id=team["team_id"], platform_id=NAF_OFFICIAL_PLATFORM_ID)
            reg_data = {
                "coach_name": team_model.get_coach().get_name(),
                "team_name": team_model.get_name(),
                "race": team_model.get_race_label(),
                "naf_name": team.get("naf_name"),
                "naf_number": team.get("naf_number")
            }
            registered_teams.append(reg_data)
        return registered_teams

    @staticmethod
    async def generate_csv_competitions(ctx):
        custom_team_db = CustomCompetitionTeam()
        tourma_file = "welsh.xml"
        round_list = TourmaXmlReader.read_tournament_file(tourma_file)
        match_list = round_list[-1]["matches"]
        round_number = len(round_list)
        team_list = custom_team_db.get_teams(205772, NAF_OFFICIAL_PLATFORM_ID)
        file1 = ""
        file2 = ""
        file3 = ""
        for table_idx, contest in enumerate(match_list, 1):
            home_team = next((item for item in team_list if item.get("members", [{}])[0].get("coach_name") == contest.get("home_coach")), None)
            away_team = next((item for item in team_list if item.get("members", [{}])[0].get("coach_name") == contest.get("away_coach")), None)

            if home_team is not None and away_team is not None:
                home_coach_id = home_team.get("members", [{}])[0].get("coach_id")
                home_team_id = home_team.get("members", [{}])[0].get("team_id")
                away_coach_id = away_team.get("members", [{}])[0].get("coach_id")
                away_team_id = away_team.get("members", [{}])[0].get("team_id")
                data = "NAF_Welsh_R{}T{},\r\n".format(round_number, table_idx)
                data += "{},RowId:{}-Server:ONLINE-StoreName:Main\r\n".format(home_coach_id, home_team_id)
                data += "{},RowId:{}-Server:ONLINE-StoreName:Main\r\n".format(away_coach_id, away_team_id)

                if table_idx not in [20, 40]:
                    data += ",\r\n"

                if table_idx < 21:
                    file1 += data
                elif table_idx < 41:
                    file2 += data
                else:
                    file3 += data
        text_file = open("tier_1_table.csv", "wt")
        text_file.write(file1)
        text_file.close()
        await ctx.channel.send(file=File("tier_1_table.csv"))

        text_file = open("tier_2_table.csv", "wt")
        text_file.write(file2)
        text_file.close()
        await ctx.channel.send(file=File("tier_2_table.csv"))

        text_file = open("tier_3_table.csv", "wt")
        text_file.write(file3)
        text_file.close()
        await ctx.channel.send(file=File("tier_3_table.csv"))

    @staticmethod
    async def create_ingame_competitions(ctx, password):
        command1 = "./comp_creator --username=SpikeBot --password={} --useIDs=True --leagueIDs=RowId:92780-Server:ONLINE-StoreName:Main --turnTimers=3 --flags=experiencedTeams,resurrection,customTeams tier_1_table.csv".format(password)
        command2 = "./comp_creator --username=SpikeBot --password={} --useIDs=True --leagueIDs=RowId:92781-Server:ONLINE-StoreName:Main --turnTimers=3 --flags=experiencedTeams,resurrection,customTeams tier_2_table.csv".format(password)
        command3 = "./comp_creator --username=SpikeBot --password={} --useIDs=True --leagueIDs=RowId:92840-Server:ONLINE-StoreName:Main --turnTimers=3 --flags=experiencedTeams,resurrection,customTeams tier_3_table.csv".format(password)

        os.system(command1)
        os.system(command2)
        os.system(command3)
