#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3
import sys

import discord
import feedparser

import discord_resources
from __init__ import spike_logger, init_log_file_handler, except_hook, intents
from spike_database.DiscordGuild import DiscordGuild
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_settings.SpikeSettings import SpikeSettings
from spike_translation import Translator
from spike_twitch.Utilities import Utilities as TwitchUtils
from discord_resources.Utilities import DiscordUtilities
from spike_utilities.Utilities import Utilities

intents.members = False
client = discord.Client(intents=intents)

# Avoid restart the notification in case of web socket restart
Already_running = False


def main():
    init_log_file_handler("video_live_notification")
    client.run(SpikeSettings.get_discord_token())


@client.event
async def on_ready():
    global Already_running
    if not Already_running:
        spike_logger.info("video_live_notification connected")
        Already_running = True
        await check_live_and_vod()
    else:
        spike_logger.warning("video_live_notification reconnect")


async def check_live_and_vod():
    await check_youtube_vod()
    await check_twitch_live()
    spike_logger.info("Notification finished")
    sys.exit()


async def check_youtube_vod():
    excluded_server = Utilities.get_excluded_server()
    rsrc = discord_resources.Resources()
    utils = DiscordUtilities(client)
    for guild in client.guilds:
        if guild.id not in excluded_server:
            db = DiscordGuild(guild.id)
            try:
                settings = DiscordGuildSettings(guild.id)
                yt_channel_list = db.get_video_channel()

                for yt_channel_id in yt_channel_list:
                    rss_url = "https://www.youtube.com/feeds/videos.xml?channel_id={}".format(yt_channel_id)
                    rss = feedparser.parse(rss_url)

                    videos = rss.entries
                    videos.reverse()

                    for data in videos:
                        if db.is_new_video(data.yt_videoid):
                            spike_logger.info("Publish video for: " + guild.name + " - id: " + str(guild.id))
                            youtube_emo = rsrc.get_youtube_emoji()
                            channel_name = "**{}**".format(data.author)
                            # /* Message when youtube video is published */
                            output_message = Translator.tr("#_video_live_notification.{youtube_emo}_{channel_name}_published_a_video_{youtube_emo}", settings.get_language()).format(
                                youtube_emo=youtube_emo, channel_name=channel_name)
                            output_message += "\n{}".format(data.title)
                            output_message += "\n{}".format(data.link)
                            out_channel = utils.get_video_channel(data.title, guild.id)
                            if out_channel is not None:
                                await utils.send_custom_message(out_channel, output_message)
                                db.save_video(data.yt_videoid)
                            else:
                                spike_logger.error("Invalid channel for {} in {}".format(data.title, guild.name))
            except sqlite3.OperationalError:
                spike_logger.info("Ignore {}".format(guild.name))
            except Exception as e:
                spike_logger.error("Unknown error: {}".format(e))


async def check_twitch_live():
    utils = DiscordUtilities(client)
    rsrc = discord_resources.Resources()
    lives_list = TwitchUtils.get_new_lives()

    for channel in lives_list:
        try:
            spike_logger.info("Notification for {} ".format(channel.get("user_name")))
            game = TwitchUtils.get_game_name(channel.get("game_id"))
            for guild_id in channel.get("discord_guild", []):
                settings = DiscordGuildSettings(guild_id)
                output_channel = client.get_channel(settings.get_live_channel())
                if output_channel is not None:

                    twitch_emo = rsrc.get_twitch_emoji()
                    channel_name = "**{}**".format(channel.get("user_name"))
                    if game is not None:
                        game = "**{}**".format(game)
                        # /* Message when twitch live start and the game is defined by the streamer*/
                        output_message = Translator.tr("#_video_live_notification.{twitch_emo}_{channel_name}_playing_{game}_on_live_{twitch_emo}", settings.get_language()).format(
                            twitch_emo=twitch_emo, channel_name=channel_name, game=game)
                    else:
                        # /* Message when twitch live start and the game is not defined by the streamer or unknown*/
                        output_message = Translator.tr("#_video_live_notification.{twitch_emo}_{channel_name}_playing_on_live_{twitch_emo}", settings.get_language()).format(twitch_emo=twitch_emo,
                                                                                                                                                                             channel_name=channel_name)
                    output_message += "\n{}".format(channel.get("title", ""))
                    output_message += "\nhttps://www.twitch.tv/{}".format(channel.get("user_name", "").lower())
                    await utils.send_custom_message(output_channel, output_message)
                else:
                    guild = client.get_guild(guild_id)
                    if guild is not None:
                        spike_logger.error("Invalid live channel for {}".format(guild.name))
                    else:
                        spike_logger.error("Invalid live channel for {}".format(guild_id))
        except Exception as e:
            spike_logger.error(e)


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
