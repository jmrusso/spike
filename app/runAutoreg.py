#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from __init__ import spike_logger, init_log_file_handler, except_hook
from spike_database.DiscordGuild import DiscordGuild
from spike_database.ResourcesRequest import ResourcesRequest
from spike_requester.CyanideApi import CyanideApi
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_utilities.Utilities import Utilities
from spike_requester.SpikeAPI.CclAPI import CclAPI


def main():
    init_log_file_handler("automatic_registration")
    automatic_registration()


def automatic_registration():
    spike_logger.info("automatic_registration start")
    excluded_server = Utilities.get_excluded_server()

    guilds_id = Utilities.get_guilds_id()

    for guild in guilds_id:
        try:
            settings = DiscordGuildSettings(guild)
            if guild not in excluded_server and settings.get_enable_autoreg():
                spike_logger.info("Automatic registration for: {}".format(guild))
                db = DiscordGuild(guild)
                db.remove_all_competition()

                common_db = ResourcesRequest()
                league_list = db.get_reported_leagues()

                for leagueId in league_list:
                    league_data = db.get_league_data(leagueId)
                    if league_data is not None:
                        platform_id = league_data[3]
                        platform_data = common_db.get_platform(platform_id)
                        if guild == 307138261825093652:
                            if platform_id == 1:
                                report_channel = 328774594485944320
                            elif platform_id == 2:
                                report_channel = 329241440666517505
                            elif platform_id == 3:
                                report_channel = 329241646099333122
                            else:
                                report_channel = 0
                        else:
                            report_channel = settings.get_autoreg_channel()
                        if report_channel > 0:
                            if guild == 307138261825093652:  # Cabal
                                season_data = CclAPI.get_current_season(platform_id=platform_id)
                                ladder_id = season_data.get("ladder", {}).get("id")
                                ladder_name = season_data.get("ladder", {}).get("name")
                                ladder_logo = common_db.get_team_emoji("Neutre_04")
                                ladder_logo_url = common_db.get_team_emoji_url("Neutre_04")
                                db.add_competition(ladder_id, ladder_name, report_channel, ladder_logo_url, ladder_logo, platform_id, league_data[1])
                                spike_logger.info("{} {} successfully added".format(platform_data[1], ladder_name))

                                playoff_id = season_data.get("playoffs", {}).get("id")
                                playoff_name = season_data.get("playoffs", {}).get("name")
                                playoff_logo = "<:ChampionCup:733314118672449576>"
                                playoff_logo_url = "https://cdn.discordapp.com/emojis/733314118672449576.png"
                                db.add_competition(playoff_id, playoff_name, report_channel, playoff_logo_url, playoff_logo, platform_id, league_data[1])
                                spike_logger.info("{} {} successfully added".format(platform_data[1], playoff_name))
                            else:
                                data = CyanideApi.get_competitions(league_data[2], platform=platform_data[1])
                                if data is not None:
                                    for competition in data["competitions"]:
                                        if competition.get("status") in [0, 1]:
                                            competition_id = competition["id"]
                                            competition_name = competition["name"]
                                            if guild == 368441333745975306:  # NAF
                                                emoji_url = "https://cdn.discordapp.com/emojis/773255569485987840.png"
                                                emoji_id = "<:welsh:773255569485987840>"
                                            else:
                                                emoji_url = common_db.get_team_emoji_url(competition["league"]["logo"])
                                                emoji_id = common_db.get_team_emoji(competition["league"]["logo"])
                                            db.add_competition(competition_id, competition_name, report_channel, emoji_url, emoji_id, platform_data[0], league_data[1])
                                            spike_logger.info("{} {} successfully added".format(platform_data[1], competition["name"]))
                                else:
                                    spike_logger.info("Unable to get competitions from **{}**.").format(league_data[2])
                        else:
                            spike_logger.info("Invalid autoreg channel {}".format(report_channel))
        except Exception as e:
            spike_logger.error(e)
    spike_logger.info("automatic_registration finished")
    sys.exit()


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
